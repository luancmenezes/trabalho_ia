import sys
import csv
from math import sqrt
from math import pow
# Dada uma matriz e um valor, encontra as coordenadas (i,j)
# que contenham o valor procurado.
def encontraPosicoes (matriz, M, N, valor):
    posicoes = []
    for i in range(0, M):
        for j in range(0, N):
            if matriz[i][j] == valor:
                posicoes.append((i, j))
    return posicoes

# Dado um estado considerado final, uma lista de predecessores e um numero de iteracao,
# apresenta em qual iteracao foi encontrada a solucao e como partir do estado inicial
# e chegar ate o estado final a partir da solucao parcial armazenada em predecessores.
def apresenta_solucao (estado, predecessores, iteracao):
    caminho = []
    caminho.append(estado)
    while predecessores[estado] != None:
        caminho.append(predecessores[estado])
        estado = predecessores[estado]
    caminho = caminho[::-1]
    print caminho

# Dado um estado qualquer e um conjunto de estados finais,
# calcula a distancia do estado qualquer ate um estado final mais proximo.
def calcula_distancia_meta (estado, estados_finais):
    x = estado[0]
    y = estado[1]
    distancia_minima = 1000000000

    for estado_final in estados_finais:
        x_estado_final = estado_final[0]
        y_estado_final = estado_final[1]
        diff1 = x_estado_final - x
        diff2 = y_estado_final - y
        somaDiffs = diff1 + diff2
        distancia_atual = somaDiffs
        if distancia_atual < distancia_minima:
            distancia_minima = distancia_atual
    return distancia_minima

# Dada uma franja (fringe) e uma funcao heuristica,
# encontra o estado com menor valor nessa franja.
def encontra_estado_mais_promissor (franja, heuristica_estados):
    valor_mais_promissor = 1000000000
    estado_mais_promissor = None
    indice_mais_promissor = 0
    indice = 0
    #print 'Franja: ' + str(franja)
    #print 'heuristica: '+ str(heuristica_estados)
    for estado in franja:
        if heuristica_estados[estado] < valor_mais_promissor:
            estado_mais_promissor = estado
            valor_mais_promissor = heuristica_estados[estado]
            indice_mais_promissor = indice
        indice = indice + 1
    #print 'Indice Promisso: ' + str(indice_mais_promissor)
    return indice_mais_promissor

def encontra_peso(matriz, var):
    return matriz[var[0]][var[1]]
def encontra_estados_sucessores_ (matriz, M, N, posicao_atual):
    i = posicao_atual[0]
    j = posicao_atual[1]
    estados_sucessores = []

    for sucessor in range(0,4):
        if matriz[i][sucessor] != '0' and sucessor > j:
            estados_sucessores.append ((i, sucessor))
    #print 'Posicao Atual ->' + str(posicao_atual)
    #print 'estados sucessores ->' + str(estados_sucessores)
    return estados_sucessores
# Algoritmo: Busca A* (A Estrela / A Star)
def busca_a_estrela (matriz, M, N, estado_inicial, estados_finais):
    distancia_meta = {}
    distancia_percorrida = {}
    heuristica = {}
    predecessores = []
    estados_expandidos = []
    solucao_encontrada = False

    # Inicializacao de distancia percorrida (f), distancia ate a meta (g) e heuristica (h = f+g).
    distancia_percorrida[estado_inicial] = 0
    distancia_meta[estado_inicial] = int(encontra_peso(matriz,estado_inicial))
    heuristica[estado_inicial] = distancia_percorrida[estado_inicial] + distancia_meta[estado_inicial]

    franja = []
    franja.append(estado_inicial)
    iteracao = 1

    while len(franja) != 0:

        #mostra_valores_franja (franja, heuristica)
        #print 'Franja: ' + str(franja) + 'Heuristica' +str (heuristica)
        indice_mais_promissor = encontra_estado_mais_promissor(franja, heuristica)
        #print  'Indice ' + str(indice_mais_promissor)
        estado = franja.pop(indice_mais_promissor)

        if estado[1] == estados_finais[0][1]:
            solucao_encontrada = True
            break

        estado = (estado[1], 0)

        estados_sucessores = encontra_estados_sucessores_(matriz, M, N, estado)

        estados_expandidos.append(estado)
        for i in range (0, len(estados_sucessores)):
            #print len(estados_sucessores)
            sucessor = estados_sucessores[i]
            if sucessor not in estados_expandidos and sucessor not in franja:
                franja.append(sucessor)
                if sucessor not in heuristica.keys():
                    distancia_meta[sucessor] = calcula_distancia_meta(sucessor, estados_finais)
                    distancia_percorrida[sucessor] = int(encontra_peso(matriz,sucessor))
                    heuristica[sucessor] = distancia_meta[sucessor] + distancia_percorrida[sucessor]
                    predecessores.append(estado[0])
        iteracao = iteracao + 1

    if solucao_encontrada == True:
        predecessores.append(estado[1])
        predecessores = list(set(predecessores))
        print predecessores

        with open('saida.csv', 'wb') as csvfile:
            spamwriter = csv.writer(csvfile, delimiter=' ',quotechar=',', quoting=csv.QUOTE_MINIMAL)
            spamwriter.writerow(predecessores)
    else:
        print "Nao foi possivel encontrar uma solucao para o problema."


# Fluxo principal do program em Python.
# Inspirado no problema 'Duende Perdido' da Olimpiada Brasileira de Informatica de 2005 - OBI 2005.
problema = open('exe.txt') # Chame o problem com: python buscas_ia.py data/duende_perdido_1.csv
leitor_problema = csv.reader (problema)
entrada = list(leitor_problema)
M = 4 # numero de linhas.
N = 4 # numero de colunas.
matriz = entrada[0:] # mapa representado como matriz.

estado_inicial = [(0, 0)]
estados_finais = [(3, 3)]
busca_a_estrela (matriz, M, N, estado_inicial[0], estados_finais)
