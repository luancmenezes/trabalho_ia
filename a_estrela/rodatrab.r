serieTS<-co2
source("trab.r")
#write.table(resultados, "resultados_co2",row.names = FALSE,col.names = FALSE)
plot.ts(co2)
saveXML(rootnode, file="co2.xml",prefix ='<?xml version="1.0" ?>')
locator(1)
#Start-> Red. Dim -> Dados Faltantes -> Decom -> EMD -> END

gap <- 300:320
serieTS<-co2
serieTS[gap]<-NA
source("trab.r")
#write.table(resultados, "resultados_co2_buracos",row.names = FALSE,col.names = FALSE)
plot.ts(serieTS)
saveXML(rootnode, file="co2_gap.xml",prefix ='<?xml version="1.0" ?>')
locator(1)
#Start-> Red. Dim -> Dados Faltantes -> GAPSSA -> Decom -> EMD -> END

set.seed(12345)
ndata <- 400
tt <- seq(0, 9, length=ndata)
meanf <- (sin(pi*tt) + sin(2*pi*tt) + sin(6*pi*tt)) * (0.0<tt & tt<=3.0) + (sin(pi*tt) + sin(6*pi*tt)) * (3.0<tt & tt<=6.0) + (sin(pi*tt) + sin(6*pi*tt) + sin(12*pi*tt)) * (6.0<tt & tt<=9.0)
snr <- 3.0
sigma <- c(sd(meanf[tt<=3]) / snr, sd(meanf[tt<=6 & tt>3]) / snr,
sd(meanf[tt>6]) / snr)
error <- c(rnorm(sum(tt<=3), 0, sigma[1]), rnorm(sum(tt<=6 & tt>3), 0, sigma[2]), rnorm(sum(tt>6), 0, sigma[3]))

serieTS <- meanf + error
source("trab.r")
#write.table(resultados, "resultados_serieRuidosa",row.names = FALSE,col.names = FALSE)
plot.ts(serieTS)
saveXML(rootnode, file="ruidosa.xml",prefix ='<?xml version="1.0" ?>')
locator(1)
# Start-> Red. Dim -> Dados Faltantes -> Decom -> SSA -> END

gap <- 300:320
serieTS<-meanf + error
serieTS[gap]<-NA
source("trab.r")
plot.ts(serieTS)
saveXML(rootnode, file="ruidosa_gap.xml",prefix ='<?xml version="1.0" ?>')
locator(1)
# Start-> Red. Dim -> Dados Faltantes -> BMDE -> Decom -> SSA -> END
