require(EMD)
library(XML)

#Dimensão
#if (dim(co2)=="NULL"){
#	d=1
#}else{
#	d=2
#}
#Tamanho da Série
if(length(serieTS)>400){
	tam=2
}else{
	tam=1
}

#Função para calcular os extremos
serieTS_extremos=extrema(serieTS[1:100])
cat("Valor dos extremos a cada 100: ",serieTS_extremos$nextreme,"\n")
if(serieTS_extremos$nextreme>45){
	extremo=2
}else{
	extremo=1
}

#Função para verificar se tem buracos na série
	num_gaps=which(is.na(serieTS))
	if (length(num_gaps)>0){
		gap=1
	}else{
		gap=0
	}

calc_pesos<- function(x,y){
	#return ((tam*x)+(extremo*y)+(d*w))
	return (1+(tam*x)+(extremo*y))
}
a=calc_pesos(2,1)
b=calc_pesos(2,3)
c=calc_pesos(1,2)
cat("Extr: ",extremo, " Tam: ",tam, " A:",a, " B:",b, " c:",c,"\n")

#Data Frame com os arestas e a função de custo de cada uma.
inicio=c("start","red.dim","red.dim","PCA","dados.faltante","dados.faltante","dados.faltante")
fim=c("red.dim","PCA","dados.faltante","dados.faltante","bmde","spline.emd","gap.ssa")
custo=c(1,1,1,1,a,b,c)
arestas=data.frame(inicio,fim,custo)
#Adicionando Linha Data frame
if (gap==0){
	custo.dados.faltantes=1
}else{
	custo.dados.faltantes=a+b+c
}
linha <- data.frame(inicio="dados.faltante",fim="decom", custo=custo.dados.faltantes)
arestas <- rbind(arestas, linha)

inicio=c("bmde","spline.emd","gap.ssa","decom","decom","emd","ssa")
fim=c("decom","decom","decom","emd","ssa","end","end")
d=calc_pesos(1,3)
e=calc_pesos(3,1)
custo=c(1,1,1,d,e,1,1)

linha <- data.frame(inicio,fim, custo)
arestas <- rbind(arestas, linha)
print(arestas)
#Criando Uma Matriz com os pesos
resultados <- matrix(data=0,nrow=11,ncol=11)
vet=c("start","red.dim","PCA","dados.faltante","bmde","spline.emd","gap.ssa","decom","emd","ssa","end")
for (i in 1:length(arestas$inicio)){
	x <- which(vet %in% arestas[i,1])
	y <- which(vet %in% arestas[i,2])
	z <- arestas[i,3]
	resultados[x,y]=z
}
#Adiciona pesos no XML
result <- xmlParse(file = "AIspace/grafo.xml")
rootnode <- xmlRoot(result)
rootnode[[1]][[21]][[3]][[1]]=a
rootnode[[1]][[22]][[3]][[1]]=b
rootnode[[1]][[23]][[3]][[1]]=c
rootnode[[1]][[27]][[3]][[1]]=custo.dados.faltantes
rootnode[[1]][[14]][[3]][[1]]=d
rootnode[[1]][[15]][[3]][[1]]=e

saveXML(rootnode, file="out.xml",prefix ='<?xml version="1.0" ?>')
